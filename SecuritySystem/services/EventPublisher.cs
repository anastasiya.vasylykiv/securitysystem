﻿using SecuritySystem.models;
using SecuritySystem.models.sensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.services
{
    public class EventPublisher
    {
        private LinkedList<Sensor> sensors;

        public EventPublisher(LinkedList<Sensor> sensors)
        {
            this.sensors = sensors;
        }

        public LinkedList<Sensor> Sensors { get; set; }

        public void notifySensors(Event curEvent) {
            foreach (Sensor s in sensors)
            {
                s.notify(curEvent);
            }
        }

        public void remove(Sensor sensor)
        {
            sensors.Remove(sensor);
        }

        public void subscribeSensor(Sensor sensor)
        {
            sensors.AddLast(sensor);
        }

    }
}
