﻿using SecuritySystem.models.building;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SecuritySystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class BuildingWindow : System.Windows.Window
    {
        public BuildingWindow(BuildingType buildingType)
        {
            InitializeComponent();
            BuildingFactory factory = new BuildingFactory();
            Building building = factory.CreateBuilding(buildingType, "123", "1");
            
            floorsBox.SelectedIndex = 0;
            for (int floor = 0; floor < building.Floors.Count; floor++)
            {
                floorsBox.Items.Add("Floor " + building.Floors[floor].Id);
            }
            for (int room = 0; room < building.Floors[0].Rooms.Count; room++)
            {
                System.Windows.Controls.Button button = new Button();
                Room currentRoom = building.Floors[0].Rooms[room];
                button.Content = currentRoom.Type.ToString();
                button.Height = 50;
                roomsPanel.Children.Add(button);              
            }

        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string floor = (sender as ComboBox).SelectedItem as string;
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Home home = new Home();
            home.Show();
            this.Close();
        }

    }
}
