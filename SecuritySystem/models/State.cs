﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class State
    {
        private long buildingId;
        private long floorId;
        private long roomId;

        public State(long buildingId, long floorId, long roomId) {
            this.buildingId = buildingId;
            this.floorId = floorId;
            this.roomId = roomId;
        }

        public long BuildingId { get; set; }
        public long FloorId { get; set; }
        public long RoomId { get; set; }

        public override string ToString()
        {
            return "building:" + buildingId + " |floor:" + floorId + " |room:" + roomId;
        }
    }
}
