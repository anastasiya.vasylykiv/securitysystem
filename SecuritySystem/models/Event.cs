﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class Event
    {
        private State state;
        private EventType type;
        private string time;

        public Event(State state, EventType type, string time)
        {
            this.state = state;
            this.type = type;
            this.time = time;
        }

        public override string ToString()
        {
            return state.ToString() +" |type:" + type + "|time: " + time;
        }

        public State State { get; set; }
        //public string Building { get; set; }
        //public string Floor { get; set; }
        //public string Room { get; set; }
        public EventType Type { get; set; }
        public string Time { get; set; }
    }
}
