﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public enum EventType
    {
        FIRE,
        INVASION,
        FLOOD,
        JEHOVAH_WITNESSES
    }
}
