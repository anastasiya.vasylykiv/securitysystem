﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class Message
    {
        private static int AUTO_ID = 0;

        private int id;
        private EventType type;
        private State state;
        private string time;

        public Message(State state, EventType type, string time)
        {
            this.id = AUTO_ID++;
            this.state = state;
            this.type = type;
            this.time = time;
        }

        public override string ToString()
        {
            return "Message" + id.ToString() + ": " + state.ToString() + " |type:" + type + "|time: " + time;
        }

        public int Id { get; set; }
        public State State { get; set; }
        //public string Building { get; set; }
        //public string Floor { get; set; }
        //public string Room { get; set; }
        public EventType Type { get; set; }
        public string Time { get; set; }
    }
}
