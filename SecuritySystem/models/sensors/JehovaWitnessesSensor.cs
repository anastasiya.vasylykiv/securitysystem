﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models.sensors
{
   public class JehovaWitnessesSensor: Sensor
    {
        public JehovaWitnessesSensor(long id, State state)
        {
            this.id = id;
            this.state = state;

        }

        public override void notify(Event curEvent)
        {
            if (EventType.JEHOVAH_WITNESSES == curEvent.Type)
            {
                //TODO: something
            }
        }
    }
}
