﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models.sensors
{
    public abstract class Sensor
    {
        protected long id;
        protected State state;

        abstract public void notify(Event curEvent);

        public State State { get; set; }
        public long Id { get; set; }
    }
}
