﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models.sensors
{
    public class InvasionSensor : Sensor
    {
       public InvasionSensor(long id, State state)
        {
            this.id = id;
            this.state = state;
            
        }

        public override void notify(Event curEvent)
        {
            if (EventType.INVASION.Equals(curEvent.Type))  //TODO: override Equals
            {
                //TODO: something
            }
        }
    }
}
