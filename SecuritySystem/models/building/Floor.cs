﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class Floor
    {
        public long Id { get; set; }

        public List<Room> Rooms { get; set; }

        public Floor() { }

        public Floor(long id, List<Room> rooms)
        {
            Id = id;
            Rooms = rooms;
        }
    }
}
