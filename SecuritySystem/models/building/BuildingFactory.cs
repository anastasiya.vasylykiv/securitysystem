﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
   public class BuildingFactory
    {
       public Building CreateBuilding (BuildingType type, string address, string postIndex)
        {
            Building building = new Building(address, postIndex);

            if (type == BuildingType.Commercial)
            {
                List<Floor> floors = new List<Floor>()
                {
                    new Floor(1,
                        new List<Room>()
                    {
                            CreateWareHouse(1, 200)
                    })
                 };
                building.Floors = floors;
            }
            else if (type == BuildingType.Residential)
            {
                List<Floor> floors = new List<Floor>()
                {
                    new Floor(1,
                        new List<Room>()
                    { CreateLivingRoom(2,50),
                      CreateKitchen(2, 30),
                      CreateHall (2, 10),
                      CreateBathroom(2, 10)
                    }),

                    new Floor(2,
                        new List<Room>()
                    { CreateLivingRoom(2,50),
                      CreateLivingRoom(2,30),
                      CreateHall (2, 10),
                      CreateBathroom(2, 10)
                    }),

                      new Floor(3,
                        new List<Room>()
                    {
                        CreateAttic(2,150)
                    
                    })
                };
                building.Floors = floors;
            }
            else
            {

                List<Floor> floors = new List<Floor>()
                {
                    new Floor(1,
                        new List<Room>()
                    {
                            CreateOpenSpace(3, 100),
                            CreateBathroom(3,15),
                            CreateBathroom(3,15),
                            CreateBathroom(3,15),
                            CreateHall(3,40)
                    })
                 };
                building.Floors = floors;
            }

            return building;
        }

        private Room CreateLivingRoom(int safetyLevel, double square)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),

                new Window(Guid.NewGuid().ToString(),safetyLevel, false),

                new Door(Guid.NewGuid().ToString(),safetyLevel, false)
            };

            return new Room(Guid.NewGuid().ToString(), safetyLevel, holes, square, "LivingRoom");
        }

        private Room CreateKitchen(int safetyLevel, double square)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),

                new Door(Guid.NewGuid().ToString(),safetyLevel, false)
            };

            return new Room(Guid.NewGuid().ToString(), safetyLevel, holes, square, "Kitchen");
        }

        private Room CreateHall(int safetyLevel, double square)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Door(Guid.NewGuid().ToString(),safetyLevel, false),

                new Door(Guid.NewGuid().ToString(),safetyLevel, false),

                new Door(Guid.NewGuid().ToString(),safetyLevel, false),

                new Door(Guid.NewGuid().ToString(),safetyLevel, false)
            };

            return new Room(Guid.NewGuid().ToString(), safetyLevel, holes, square, "Hall");
        }

        private Room CreateWareHouse(int safetyLevel, double square)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Door(Guid.NewGuid().ToString(),safetyLevel, false),

                new Window(Guid.NewGuid().ToString(),safetyLevel, false),

                new Window(Guid.NewGuid().ToString(),safetyLevel, false),

                new Window(Guid.NewGuid().ToString(),safetyLevel, false)
            };

            return new Room(Guid.NewGuid().ToString(), safetyLevel, holes, square, "WareHouse");
        }

        private Room CreateBathroom(int safetyLevel, double square)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Door(Guid.NewGuid().ToString(),safetyLevel, false)

            };

            return new Room(Guid.NewGuid().ToString(), safetyLevel, holes, square, "Bathroom");
        }

        private Room CreateAttic(int safetyLevel, double square)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Door(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false)
            };

            return new Room(Guid.NewGuid().ToString(), safetyLevel, holes, square, "Attic");
        }

        private Room CreateOpenSpace(int safetyLevel, double square)
        {
            List<Hole> holes = new List<Hole>()
            {
                new Door(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false),
                new Window(Guid.NewGuid().ToString(),safetyLevel, false)
            };

            return new Room(Guid.NewGuid().ToString(), safetyLevel, holes, square, "OpenSpace");
        }
    }
}
