﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public interface ISensorOwner
    {
        string Id { get; set; }

        int SafetyLevel { get; set; }

        bool IsSecured { get; set; }


    }
}
