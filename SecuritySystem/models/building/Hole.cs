﻿using System;

namespace SecuritySystem.models.building
{
    public abstract class Hole : ISensorOwner
    {
       public string Id { get; set; }

       public int SafetyLevel { get; set; }

       public bool IsOpen { get; set; }

       public bool IsSecured { get; set; }  //checks if it has a sensor inside
     
       public Hole() { }

       public Hole (string id, int safetyLevel, bool isOpen)
       {
            Id = id;
            SafetyLevel = safetyLevel;
            IsOpen = isOpen;
       }
    }
}
