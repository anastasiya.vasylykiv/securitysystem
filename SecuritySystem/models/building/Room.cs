﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class Room : ISensorOwner
    {
        public string Id { get; set; }

        public int SafetyLevel { get; set; }

        public bool IsSecured { get; set; }

        public List<Hole> Holes;

        public double Square { get; set; }

        // public Dictionary<ISensorOwner, T> Sensors;  //change T to Sensors

        public string Type { get; set; }

        public Room () { }

        public Room (string id, int safetyLevel, List<Hole> holes, double square, string type)
        {
            Id = id;
            SafetyLevel = safetyLevel;
            Holes = holes;
            Square = square;
            Type = type;
        }
    }
}
