﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SecuritySystem.models.building
{
    public class Building
    {
        public List<Floor> Floors { get; set; }

        public string Address { get; set; }

        public string PostIndex { get; set; }

        public Building() { }

        public Building (string address, string postIndex)
        {
            Address = address;
            PostIndex = postIndex;
        }
    }
}
