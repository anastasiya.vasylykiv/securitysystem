﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.models
{
    public class Dispatcher
    {
        private Queue<Message> queue;

        public Dispatcher()
        {
            this.queue = new Queue<Message>();
        }

        public void dispatchMessage(Message message)
        {
            this.queue.Enqueue(message);
        }

        public Message getFirstMessage()
        {
            return this.queue.First();
        }

        public Message getFirstMessageAndRemove()
        {
            Message message = this.queue.Dequeue();
            return message;
        }


        public Message getLastMessage()
        {
            return this.queue.Last();
        }

    }
}
