﻿using SecuritySystem.models.building;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SecuritySystem
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : System.Windows.Window
    {
        public Home()
        {
            InitializeComponent();      
        }

        private void Type_Click(object sender, RoutedEventArgs e)
        {
            BuildingType type = (BuildingType) System.Enum.Parse(typeof(BuildingType), (sender as Button).Name.ToString());
            BuildingWindow building = new BuildingWindow(type);
            building.Show(); 
            this.Close();

        }
    }
}
